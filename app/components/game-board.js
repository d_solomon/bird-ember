import Ember from 'ember';


function movePlayerInDirection({x, y}, gState) {
  let newPos = {
    x: gState.playerInfo.x + x,
    y: gState.playerInfo.y + y,
  };

  if (isWallAt(newPos, gState)) {
    return null;
  }

  let formInfo = getFormInfoAt(newPos, gState);
  let objectsToMove = {};

  if (formInfo !== undefined) {
    let newFormPos = {
      x: formInfo.x + x,
      y: formInfo.y + y,
      formId: formInfo.formId
    };

    if (isWallOrFormAt(newFormPos, gState)) {
      return null;
    }

    objectsToMove.form = newFormPos;
  }
  objectsToMove.player = newPos;

  return objectsToMove;
}

function isWallAt(pos, gState) {
  return isTileTypeAt(pos, "wall", gState);
}

function isWallOrFormAt(pos, gState) {
  return isWallAt(pos, gState) || (getFormInfoAt(pos, gState) !== undefined);
}

function isFinishAt(pos, gState) {
  return isTileTypeAt(pos, "finish", gState);
}

function isTileTypeAt({x, y}, tileType, gState) {
  for (let tileState of gState.tiles) {
    if (x === tileState.x && y === tileState.y && tileType === tileState.type) {
      return true;
    }
  }

  return false;
}

function getFormInfoAt({x, y}, gState) {
  for (let formInfo of gState.formInfos) {
    if (x === formInfo.x && y === formInfo.y) {
      return formInfo;
    }
  }
  return undefined;
}

function isGameWon(gState) {
  for (let formInfo of gState.formInfos) {
    if (!isFinishAt(formInfo, gState)) {
      return false;
    }
  }
  return true;
}

// function checkGameWon(gState) {
//   if (isGameWon(gState)) {
//     alert("wow you win!!");
//     // window.location.href = '/static/win.html';
//   }
// }

export default Ember.Component.extend({
  didRender: function () {
    this.$().attr({tabindex: 1});
    this.$().focus();
  },
  gameState: Ember.inject.service('game-state'),
  showWin: false,


  keyDown(e) {
    // todo: move to const place
    var KEY_LEFT = 37;
    var KEY_UP = 38;
    var KEY_RIGHT = 39;
    var KEY_DOWN = 40;

    let keyCode = e.keyCode;

    if (keyCode === KEY_LEFT ||
      keyCode === KEY_UP ||
      keyCode === KEY_RIGHT ||
      keyCode === KEY_DOWN) {
      let direction;

      switch (e.keyCode) {
        case KEY_LEFT:
          direction = {x: -1, y: 0};
          break;
        case KEY_RIGHT:
          direction = {x: 1, y: 0};
          break;
        case KEY_UP:
          direction = {x: 0, y: -1};
          break;
        case KEY_DOWN:
          direction = {x: 0, y: 1};
          break;
      }

      let gState = this.get('gameState');
      let objectsToMove = movePlayerInDirection(direction, gState);
      if (objectsToMove != null) {
        if (objectsToMove.form != null) {
          this.get('gameState').moveForm(objectsToMove.form);
        }
        this.get('gameState').movePlayer(objectsToMove.player);
      }
      // let won = checkGameWon(gState);
      if (isGameWon(gState)) {
        // alert("wow you win!!");
        this.set('showWin', true);
        // window.location.href = '/static/win.html';
      }
    }
  },
});
