import Ember from 'ember';

export function gameTileLeft([x]) {
  return x * 80;
}

export default Ember.Helper.helper(gameTileLeft);
