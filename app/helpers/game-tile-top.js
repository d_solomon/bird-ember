import Ember from 'ember';

export function gameTileTop([y]) {
  return y * 80;
}

export default Ember.Helper.helper(gameTileTop);
