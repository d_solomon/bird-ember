import Ember from 'ember';

function cloneJson(json) {
  return JSON.parse(JSON.stringify(json));
}

export default Ember.Service.extend({
  generalInfo: null,
  tiles: null,
  playerInfo: null,
  formInfos: [],

  init() {
    let initgGeneralInfo = {
      width: 6,
      height: 7,
      moves: 0
    };
    this.set('generalInfo', initgGeneralInfo);

    let initTiles = [
      {x: 0, y: 0, type: "nothing"},
      {x: 1, y: 0, type: "wall"},
      {x: 2, y: 0, type: "wall"},
      {x: 3, y: 0, type: "wall"},
      {x: 4, y: 0, type: "wall"},
      {x: 5, y: 0, type: "wall"},

      {x: 0, y: 1, type: "nothing"},
      {x: 1, y: 1, type: "wall"},
      {x: 2, y: 1, type: "floor"},
      {x: 3, y: 1, type: "floor"},
      {x: 4, y: 1, type: "floor"},
      {x: 5, y: 1, type: "wall"},

      {x: 0, y: 2, type: "wall"},
      {x: 1, y: 2, type: "wall"},
      {x: 2, y: 2, type: "wall"},
      {x: 3, y: 2, type: "floor"},
      {x: 4, y: 2, type: "floor"},
      {x: 5, y: 2, type: "wall"},

      {x: 0, y: 3, type: "wall"},
      {x: 1, y: 3, type: "floor"},
      {x: 2, y: 3, type: "floor"},
      {x: 3, y: 3, type: "floor"},
      {x: 4, y: 3, type: "floor"},
      {x: 5, y: 3, type: "wall"},

      {x: 0, y: 4, type: "wall"},
      {x: 1, y: 4, type: "floor"},
      {x: 2, y: 4, type: "floor"},
      {x: 3, y: 4, type: "floor"},
      {x: 4, y: 4, type: "wall"},
      {x: 5, y: 4, type: "wall"},

      {x: 0, y: 5, type: "wall"},
      {x: 1, y: 5, type: "floor"},
      {x: 2, y: 5, type: "floor"},
      {x: 3, y: 5, type: "floor"},
      {x: 4, y: 5, type: "wall"},
      {x: 5, y: 5, type: "nothing"},

      {x: 0, y: 6, type: "wall"},
      {x: 1, y: 6, type: "wall"},
      {x: 2, y: 6, type: "wall"},
      {x: 3, y: 6, type: "wall"},
      {x: 4, y: 6, type: "wall"},
      {x: 5, y: 6, type: "nothing"},

      {x: 2, y: 1, type: "finish"},
      {x: 3, y: 1, type: "finish"},
      {x: 2, y: 4, type: "form", formId: 1},
      {x: 2, y: 3, type: "form", formId: 2},
      {x: 1, y: 5, type: "player", player: true}
    ];
    this.set('tiles', initTiles);
    let initPlayerInfo = {x: 1, y: 5, type: "player"}
    this.set('playerInfo', initPlayerInfo);
    let initFormInfos = [
      {x: 2, y: 4, type: "form", formId: 1},
      {x: 2, y: 3, type: "form", formId: 2}
    ];
    this.set('formInfos', initFormInfos);

  },
  getPlayerInfo() {
    return this.get("playerInfo");
  },

  moveForm(newFormPos) {
    let updatedTiles = cloneJson(this.get("tiles"));
    for (let tile of updatedTiles) {
      if (tile.type === "form" && tile.formId == newFormPos.formId) {
        tile.x = newFormPos.x;
        tile.y = newFormPos.y;
        break;
      }
    }
    this.set("tiles", updatedTiles);

    let updateFormInfo = cloneJson(this.get("formInfos"));
    for (let formInfo of updateFormInfo) {
      if (formInfo.formId == newFormPos.formId) {
        formInfo.x = newFormPos.x;
        formInfo.y = newFormPos.y;
        break;
      }
    }
    this.set("formInfos", updateFormInfo);

  },
  movePlayer({x, y}) {
    let updatedTiles = cloneJson(this.get("tiles"));
    for (let tile of updatedTiles) {
      if (tile.type === "player") {
        tile.x = x;
        tile.y = y;
        break;
      }
    }
    this.set("tiles", updatedTiles);

    let updatePlayerInfo = cloneJson(this.get("playerInfo"));
    updatePlayerInfo.x = x;
    updatePlayerInfo.y = y;
    this.set("playerInfo", updatePlayerInfo);
  }
});
