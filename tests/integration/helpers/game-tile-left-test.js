
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('game-tile-left', 'helper:game-tile-left', {
  integration: true
});

// Replace this with your real tests.
test('it renders', function(assert) {
  this.set('inputValue', '1234');

  this.render(hbs`{{game-tile-left inputValue}}`);

  assert.equal(this.$().text().trim(), '1234');
});

